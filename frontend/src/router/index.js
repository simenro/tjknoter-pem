import { createRouter, createWebHistory } from "vue-router";

import LoginDialog from "@/components/LoginDialog.vue";
import Instrument from "@/components/InstrumentDialog.vue";
import Noter from "@/components/noter/Noter";
import Nedlastinger from "@/components/noter/Nedlastinger";

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "LoginDialog",
      component: LoginDialog,
    },
    {
      path: "/instrument/",
      name: "Instrument",
      component: Instrument,
    },
    {
      path: "/noter/",
      name: "Noter",
      component: Noter,
    },
    {
      path: "/nedlastinger/",
      name: "Nedlastinger",
      component: Nedlastinger,
    },
  ],
});
